package Formated_key_print;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class WriteInstructions {
    private String BEGIN_CONSTANT = "---BEGIN OS2 CRYPTO DATA---";
    private String END_CONSTANT   = "---END OS2 CRYPTO DATA---";
    private Attributes attributes;

    public WriteInstructions() {
        attributes = new Attributes();
    }

    public WriteInstructions(Attributes attributes) {
        this.attributes = attributes;
    }

    public void toFile(String stringPath) throws IOException {
        Files.write(Paths.get(stringPath), attributes.getAllLines());
    }



}
