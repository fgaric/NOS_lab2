package Formated_key_print;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class ReadInstructions {
    private String BEGIN_CONSTANT = "---BEGIN OS2 CRYPTO DATA---";
    private String END_CONSTANT   = "---END OS2 CRYPTO DATA---";
    private Attributes attributes;

    public ReadInstructions() {
        attributes = new Attributes();
    }


    public void fromFile(String stringPath) throws IOException {
        List<String> allLinesFromFile = Files.readAllLines(Paths.get(stringPath)); //TODO vidjet dal zadat ASCII
        if (!checkValidity(allLinesFromFile)) {
            System.out.println("Wrongly formatted file");
            System.exit(1);
        }
        updateAttributesFromFile(allLinesFromFile);
    }

    private void updateAttributesFromFile(List<String> allLinesFromFile) {
        //allLinesFromFile.remove(0);
        //allLinesFromFile.remove(allLinesFromFile.size()-1);

        for (int i=0; i<allLinesFromFile.size(); i++) {
            if (allLinesFromFile.get(i).startsWith(" ") || allLinesFromFile.get(i).isEmpty() || allLinesFromFile.get(i).startsWith(BEGIN_CONSTANT)) {
                continue;
            }
            for (String attributeName : attributes.getAttributeNameList()) {
                if (allLinesFromFile.get(i).replace(":", "").toLowerCase().trim().equals(attributeName)) {
                    attributes.switchForNow(attributeName.toLowerCase(), allLinesFromFile.get(i+1).trim());
                    updateSameAttributeIfMoreData(attributes, attributeName, allLinesFromFile, i);
                }
            }
        }
    }

    private boolean checkValidity(List<String> allLines) {
        return allLines.get(0).equals(BEGIN_CONSTANT) && allLines.get(allLines.size() - 1).equals(END_CONSTANT);
    }

    public Attributes getAttributes() {
        return attributes;
    }

    private void updateSameAttributeIfMoreData(Attributes attributes, String attributeName, List<String> allLinesFromFile, int i) {
        for (int j=i+2; j<allLinesFromFile.size(); j++) {
            if (allLinesFromFile.get(j).startsWith("    ")) {
                attributes.switchForNow(attributeName.toLowerCase(), allLinesFromFile.get(j).trim());
            } else {
                break;
            }
        }
    }
}
