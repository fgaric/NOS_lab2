package Formated_key_print;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

public class Attributes {
    private String BEGIN_CONSTANT = "---BEGIN OS2 CRYPTO DATA---";
    private String END_CONSTANT   = "---END OS2 CRYPTO DATA---";
    private String description;
    private String fileName;
    private String method;
    private String keyLength;
    private String secretKey;
    private String initializationVector;
    private String modulus;
    private String publicExponent;
    private String privateExponent;
    private String signature;
    private String data;
    private String envelopeData;
    private String envelopeCryptKey;

    private ArrayList<String> attributeList;

    public Attributes() {
        attributeList = new ArrayList<>();
        attributeList.add("description");
        attributeList.add("file name");
        attributeList.add("method");
        attributeList.add("key length");
        attributeList.add("secret key");
        attributeList.add("initialization vector");
        attributeList.add("modulus");
        attributeList.add("public exponent");
        attributeList.add("private exponent");
        attributeList.add("signature");
        attributeList.add("data");
        attributeList.add("envelope data");
        attributeList.add("envelope crypt key");
    }

    public void switchForNow(String attribute, String attributeData) {
        switch (attribute) {
            case "description"          : setDescription(attributeData); break;
            case "file name"             : setFileName(attributeData);break;
            case "method"               : setMethod(attributeData);break;
            case "key length"            : setKeyLength(attributeData);break;
            case "secret key"            : setSecretKey(attributeData);break;
            case "initialization vector" : setInitializationVector(attributeData);break;
            case "modulus"              : setModulus(attributeData);break;
            case "public exponent"       : setPublicExponent(attributeData);break;
            case "private exponent"      : setPrivateExponent(attributeData);break;
            case "signature"            : setSignature(attributeData);break;
            case "data"                 : setData(attributeData);break;
            case "envelope data"         : setEnvelopeData(attributeData);break;
            case "envelope crypt key"     : setEnvelopeCryptKey(attributeData);break;
            default                     : break;
        }
    }

    public List<String> getAllLines() {
        ArrayList<String> lines = new ArrayList<>();
        lines.add(BEGIN_CONSTANT);

        for (String s : attributeList) {
            switch (s) {
                case "description":
                    if (getDescription() != null) {
                        lines.add("Description:");
                        lines.add(addSpaces() + getDescription());
                        lines.add("");
                    } break;
                case "file name":
                    if (getFileName() != null) {
                        lines.add("File name:");
                        lines.add(addSpaces() + getFileName());
                        lines.add("");
                    } break;
                case "method":
                    if (getMethod() != null) {
                        lines.add("Method:");
                        lines.add(addSpaces() + getMethod());
                        lines.add("");
                    } break;
                case "key length":
                    if (getKeyLength() != null) {
                        lines.add("Key length:");
                        lines.add(addSpaces() + getKeyLength());
                        lines.add("");
                    } break;
                case "secret key":
                    if (getSecretKey() != null) {
                        lines.add("Secret Key:");
                        lines.add(addSpaces() + getSecretKey());
                        lines.add("");
                    } break;
                case "initialization vector":
                    if (getInitializationVector() != null) {
                        lines.add("Initialization vector:");
                        lines.add(addSpaces() + getInitializationVector());
                        lines.add("");
                    } break;
                case "modulus":
                    if (getModulus() != null) {
                        lines.add("Modulus:");
                        lines.add(addSpaces() + getModulus());
                        lines.add("");
                    } break;
                case "public exponent":
                    if (getPublicExponent() != null) {
                        lines.add("Public exponent:");
                        lines.add(addSpaces() + getPublicExponent());
                        lines.add("");
                    } break;
                case "private exponent":
                    if (getPrivateExponent() != null) {
                        lines.add("Private exponent:");
                        lines.add(addSpaces() + getPrivateExponent());
                        lines.add("");
                    } break;
                case "signature":
                    if (getSignature() != null) {
                        lines.add("Signature:");
                        lines.addAll(get60Style(this.getSignature()));
                    } break;
                case "data":
                    if (getData() != null) {
                        lines.add("Data:");
                        lines.addAll(get60Style(this.data));
                    } break;
                case "envelope data":
                    if (getEnvelopeData() != null) {
                        lines.add("Envelope data:");
                        lines.addAll(get60Style(this.getEnvelopeData()));
                    } break;
                case "envelope crypt key":
                    if (getEnvelopeCryptKey() != null) {
                        lines.add("Envelope crypt key:");
                        lines.addAll(get60Style(this.getEnvelopeCryptKey()));
                    } break;
                default:
                    break;
            }
        }

        lines.add("");
        lines.add(END_CONSTANT);
        return lines;
    }

    public ArrayList<String> getAttributeNameList() {
        return attributeList;
    }

    public void setAttributeList(ArrayList<String> attributeList) {
        this.attributeList = attributeList;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method =  method;
    }

    public String getKeyLength() {
        return keyLength;
    }

    public void setKeyLength(String keyLength) {
        this.keyLength = keyLength;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getInitializationVector() {
        return initializationVector;
    }

    public void setInitializationVector(String initializationVector) {
        this.initializationVector = initializationVector;
    }

    public String getModulus() {
        return modulus;
    }

    public void setModulus(String modulus) {
        if (this.modulus == null) {
            this.modulus = modulus;
        } else {
            this.modulus += modulus;
        }
    }

    public String getPublicExponent() {
        return publicExponent;
    }

    public void setPublicExponent(String publicExponent) {
        if(this.publicExponent == null) {
            this.publicExponent = publicExponent;
        } else {
            this.publicExponent += publicExponent;
        }
    }

    public String getPrivateExponent() {
        return privateExponent;
    }

    public void setPrivateExponent(String privateExponent) {
        if (this.privateExponent == null) {
            this.privateExponent = privateExponent;
        } else {
            this.privateExponent += privateExponent;
        }
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        if (this.signature == null) {
            this.signature = signature;
        } else {
            this.signature += signature;
        }
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        if (this.data == null) {
            this.data = data;
        } else {
            this.data += data;
        }
    }

    public void setData(byte[] data) {
        //this.data = Base64.getEncoder().encodeToString(data);
        this.data = Base64.getEncoder().encodeToString(data);
    }

    public String getEnvelopeData() {
        return envelopeData;
    }

    public void setEnvelopeData(String envelopeData) {
        if (this.envelopeData == null) {
            this.envelopeData = envelopeData;
        } else {
            this.envelopeData += envelopeData;
        }
    }

    public String getEnvelopeCryptKey() {
        return envelopeCryptKey;
    }

    public void setEnvelopeCryptKey(String envelopeCryptKey) {
        if (this.envelopeCryptKey == null) {
            this.envelopeCryptKey = envelopeCryptKey;
        } else {
            this.envelopeCryptKey += envelopeCryptKey;
        }
    }

    private String addSpaces() {
        return "    ";
    }

    private List<String> get60Style(String data) {
        ArrayList<String> lines = new ArrayList<>();

        if (data.length() <= 60) {
            lines.add(addSpaces() + data);
            lines.add("");
        } else {
            int i = 0;
            int temp = -1;
            for (; i<data.length(); i++) {
                if (i != 0 && i % 59 == 0) {
                    lines.add(addSpaces() + data.substring(temp+1, i));
                    temp = i;
                }
            }
            lines.add(addSpaces() + data.substring(temp+1, i));
        }

        return lines;
    }
}
