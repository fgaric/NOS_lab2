import Digital_stuff.DigitalEnvelope;
import Digital_stuff.DigitalSignature;
import Digital_stuff.DigitalStamp;
import Utils.AESUtil;
import Utils.CoolUtil;
import Utils.Des3xUtil;
import Utils.RSAUtil;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;

public class Main {



    public static void main(String[] args) throws Exception {

/*        byte[] IV = new byte[16];
        SecureRandom random = new SecureRandom();
        random.nextBytes(IV);

        StringBuilder sb = new StringBuilder();
        for (byte b : IV) {
            sb.append(String.format("%02x", b));
        }
        String IVinHex = sb.toString();
        System.out.println(IVinHex);

        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        String privatniHexa = CoolUtil.bytesToHex(keyPair.getPrivate().getEncoded());
        String javniHexa    = CoolUtil.bytesToHex(keyPair.getPublic().getEncoded());*/




        int i = 5;

        switch (i) {
            case 1 : {  String rsa_input       = "C:\\noslab2\\rsa_ulaz.txt";
                        String rsa_output      = "C:\\noslab2\\rsa_izlaz.txt";
                        String rsa_public_key  = "C:\\noslab2\\rsa_kljuc_javni.txt";
                        String rsa_private_key = "C:\\noslab2\\rsa_kljuc_privatni.txt";

                        RSAUtil rsaUtil = new RSAUtil(rsa_input, rsa_output, rsa_public_key, rsa_private_key);
                        rsaUtil.encrypt();
                        rsaUtil.decrypt();
            } break;

            case 2 : {
                        String aes_input  = "C:\\noslab2\\aes_ulaz.txt";
                        String aes_output = "C:\\noslab2\\aes_izlaz.txt";
                        String aes_key    = "C:\\noslab2\\aes_kljuc.txt";

                        AESUtil aesUtil = new AESUtil(aes_input, aes_output, aes_key);
                        aesUtil.encrypt();
                        aesUtil.decrypt();
            } break;

            case 3 : {
                        String des3x_input  = "C:\\noslab2\\des3x_ulaz.txt";
                        String des3x_output = "C:\\noslab2\\des3x_izlaz.txt";
                        String des3x_key    = "C:\\noslab2\\des3x_kljuc.txt";

                        Des3xUtil des3xUtil = new Des3xUtil(des3x_input, des3x_output, des3x_key);
                        des3xUtil.encrypt();
                        des3xUtil.decrypt();
            } break;

            case 4 : {
                        String envelope_input       = "C:\\noslab2\\des3x_ulaz.txt";
                        String envelope_symetricKey = "C:\\noslab2\\des3x_kljuc.txt";
                        String envelope_publicKey   = "C:\\noslab2\\rsa_kljuc_javni.txt";
                        String envelope_output      = "C:\\noslab2\\omotnica_izlaz.txt";

                        DigitalEnvelope digitalEnvelope = new DigitalEnvelope(envelope_input, envelope_symetricKey, envelope_publicKey, envelope_output);
                        digitalEnvelope.createEnvelope();
            } break;

            case 5 : {
                        String signature_input      = "C:\\noslab2\\potpis_ulaz.txt";
                        String signature_privateKey = "C:\\noslab2\\potpis_kljuc_privatni.txt";
                        String signature_output     = "C:\\noslab2\\potpis_izlaz.txt";

                        DigitalSignature digitalSignature = new DigitalSignature(signature_input, signature_privateKey, signature_output);
                        digitalSignature.createSignature();
            } break;
            case 6 : {
                        String stamp_input        = "C:\\noslab2\\pecat_ulaz.txt";
                        String stamp_symetricKey  = "C:\\noslab2\\pecat_simetricni_kljuc.txt";
                        String stamp_publicKey    = "C:\\noslab2\\pecat_javni_kljuc.txt";
                        String stamp_privateKey   = "C:\\noslab2\\pecat_privatni_kljuc.txt";
                        String stamp_output       = "C:\\noslab2\\pecat_izlaz.txt";

                        DigitalStamp digitalStamp = new DigitalStamp(stamp_input, stamp_symetricKey, stamp_publicKey, stamp_privateKey, stamp_output);
                        digitalStamp.createStamp();
            } break;
        }

    }
}
