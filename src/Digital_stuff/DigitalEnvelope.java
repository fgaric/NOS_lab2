package Digital_stuff;

import Formated_key_print.Attributes;
import Formated_key_print.ReadInstructions;
import Formated_key_print.WriteInstructions;
import Utils.Des3xUtil;
import Utils.EnvelopeObject;
import Utils.RSAUtil;

public class DigitalEnvelope {

    private String envelope_input;
    private String envelope_symetricKey;
    private String envelope_publicKey;
    private String envelope_output;

    public DigitalEnvelope(String envelope_input, String envelope_symetricKey, String envelope_publicKey, String envelope_output) {
        this.envelope_input       = envelope_input;
        this.envelope_symetricKey = envelope_symetricKey;
        this.envelope_publicKey   = envelope_publicKey;
        this.envelope_output      = envelope_output;
    }

    public EnvelopeObject createEnvelope() throws Exception {
        String des3x_input  = "C:\\noslab2\\des3x_ulaz.txt";
        String des3x_output = "C:\\noslab2\\des3x_izlaz.txt";
        Des3xUtil des3xUtil = new Des3xUtil(des3x_input, des3x_output, envelope_symetricKey);
        String encryptedWithSymKey = des3xUtil.encrypt();

        ReadInstructions ri = new ReadInstructions();
        ri.fromFile(envelope_symetricKey);
        String symetricKey = ri.getAttributes().getSecretKey();

        String rsa_output      = "C:\\noslab2\\rsa_izlaz.txt";
        String rsa_private_key = "C:\\noslab2\\rsa_kljuc_privatni.txt";
        RSAUtil rsaUtil = new RSAUtil(symetricKey, rsa_output, envelope_publicKey, rsa_private_key);
        String encryptedSymKey = rsaUtil.encrypt();

        writeEnvelopeToFile(encryptedWithSymKey, encryptedSymKey);

        return new EnvelopeObject(encryptedWithSymKey, encryptedSymKey);

    }

    private void writeEnvelopeToFile(String envelopeData, String envelopeCryptKey) throws Exception {
        Attributes attributes = new Attributes();

        attributes.setDescription("Envelope");
        attributes.setMethod("3-Des, RSA");
        attributes.setKeyLength("40, 0200");
        attributes.setEnvelopeData(envelopeData);
        attributes.setEnvelopeCryptKey(envelopeCryptKey);
        new WriteInstructions(attributes).toFile(envelope_output);

    }
}
