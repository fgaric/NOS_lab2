package Digital_stuff;

import AES_3DES_RSA_SHA.SHA;
import Formated_key_print.Attributes;
import Formated_key_print.WriteInstructions;
import Utils.RSAUtil;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DigitalSignature {

    private String signature_input;
    private String signature_privateKey;
    private String signature_output;
    private SHA sha;

    public DigitalSignature(String signature_input, String signature_privateKey, String signature_output) {
        this.signature_input = signature_input;
        this.signature_privateKey = signature_privateKey;
        this.signature_output = signature_output;
        sha = new SHA();
    }

    public String createSignature() throws Exception{
        String file = new String(Files.readAllBytes(Paths.get(signature_input)));
        String hashed = sha.shaThisString(file);

        String rsa_output    = "C:\\noslab2\\nebitno.txt";
        String rsa_publicKey = "C:\\noslab2\\rsa_kljuc_javni.txt";
        RSAUtil rsaUtil      = new RSAUtil(hashed, rsa_output, rsa_publicKey, signature_privateKey);
        String signature     = rsaUtil.encrypt();

        writeSignatureToFile(signature);

        return signature;

    }

    private void writeSignatureToFile(String signatureData) throws Exception {
        Attributes attributes = new Attributes();
        attributes.setDescription("Signature");
        attributes.setFileName(signature_input);
        attributes.setMethod("SHA-224, RSA");
        attributes.setKeyLength("40, 0200");
        attributes.setSignature(signatureData);
        new WriteInstructions(attributes).toFile(signature_output);

    }

}
