package Digital_stuff;

import Formated_key_print.Attributes;
import Formated_key_print.WriteInstructions;
import Utils.EnvelopeObject;

public class DigitalStamp {
    private String stamp_input;
    private String stamp_symetricKey;
    private String stamp_publicKey;
    private String stamp_privateKey;
    private String stamp_output;

    public DigitalStamp(String stamp_input, String stamp_symetricKey, String stamp_publicKey, String stamp_privateKey, String stamp_output) {
        this.stamp_input = stamp_input;
        this.stamp_symetricKey = stamp_symetricKey;
        this.stamp_publicKey = stamp_publicKey;
        this.stamp_privateKey = stamp_privateKey;
        this.stamp_output = stamp_output;
    }

    public void createStamp() throws Exception {
/*        String output = "C:\\noslab2\\des3x_izlaz.txt";
        Des3xUtil des3xUtil = new Des3xUtil(stamp_input, output, stamp_symetricKey);
        String C1 = des3xUtil.encrypt();

        String rsa_output = "C:\\noslab2\\rsa_izlaz.txt";
        RSAUtil rsaUtil = new RSAUtil(C1, output, stamp_publicKey, stamp_privateKey); //javnim kljucem
        String C2 = rsaUtil.encrypt();*/

        String envelope_input       = "C:\\noslab2\\des3x_ulaz.txt";
        String envelope_symetricKey = "C:\\noslab2\\des3x_kljuc.txt";
        String envelope_publicKey   = "C:\\noslab2\\rsa_kljuc_javni.txt";
        String envelope_output      = "C:\\noslab2\\omotnica_izlaz.txt";
        DigitalEnvelope digitalEnvelope = new DigitalEnvelope(envelope_input, envelope_symetricKey, envelope_publicKey, envelope_output);
        EnvelopeObject eo = digitalEnvelope.createEnvelope();


        String signature_input      = "C:\\noslab2\\potpis_ulaz.txt";
        String signature_privateKey = "C:\\noslab2\\potpis_kljuc_privatni.txt";
        String signature_output     = "C:\\noslab2\\potpis_izlaz.txt";
        DigitalSignature digitalSignature = new DigitalSignature(signature_input, signature_privateKey, signature_output);
        String C3 = digitalSignature.createSignature();

        updateStampToFile(eo, C3);

    }

    private void updateStampToFile(EnvelopeObject eo, String C3) throws Exception{
        Attributes attributes = new Attributes();
        attributes.setDescription("Digital Stamp");
        attributes.setFileName(stamp_input);
        attributes.setMethod("DES, RSA, SHA-1");
        attributes.setEnvelopeData(eo.getC1());
        attributes.setEnvelopeCryptKey(eo.getC2());
        attributes.setSignature(C3);

        new WriteInstructions(attributes).toFile(stamp_output);
    }
}
