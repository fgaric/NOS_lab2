package Utils;

import AES_3DES_RSA_SHA.RSA;
import Formated_key_print.Attributes;
import Formated_key_print.ReadInstructions;
import Formated_key_print.WriteInstructions;
import com.sun.prism.shader.AlphaTexture_RadialGradient_AlphaTest_Loader;
import jdk.nashorn.internal.runtime.ECMAException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

@SuppressWarnings("Duplicates")
public class RSAUtil {
    private String rsa_input;
    private String rsa_output;
    private String rsa_public_key;
    private String rsa_private_key;
    private RSA rsa;

    public RSAUtil(String rsa_input, String rsa_output, String rsa_public_key, String rsa_private_key) {
        this.rsa_input = rsa_input;
        this.rsa_output = rsa_output;
        this.rsa_public_key = rsa_public_key;
        this.rsa_private_key = rsa_private_key;
        rsa = new RSA();
    }

    public String encrypt() throws Exception{
        ReadInstructions ri = new ReadInstructions();
        ri.fromFile(rsa_public_key);
        String keyLength      = ri.getAttributes().getKeyLength();
        String modulus        = ri.getAttributes().getModulus();
        String publicExponent = ri.getAttributes().getPublicExponent();

        ReadInstructions ri1 = new ReadInstructions();
        ri1.fromFile(rsa_private_key);
        String privateExponent = ri1.getAttributes().getPrivateExponent();

        if (rsa_input.startsWith("C:")) {
            String fileText = new String(Files.readAllBytes(Paths.get(rsa_input)));
            rsa.encrypt(fileText, rsa.getPublicKey(publicExponent, modulus));
        } else if (!rsa_output.equals("C:\\noslab2\\nebitno.txt")){
            rsa.encrypt(rsa_input, rsa.getPublicKey(publicExponent, modulus));
        } else {
            rsa.encrypt(rsa_input, rsa.getPrivateKey(privateExponent, modulus));
        }

        return updateEncryptAttributes();
    }

    public void decrypt() throws Exception {
        ReadInstructions ri = new ReadInstructions();
        ri.fromFile(rsa_output);
        String data_in_Base64 = ri.getAttributes().getData();
        //byte[] data_in_byte   = Base64.getDecoder().decode(data_in_Base64);


        ReadInstructions ri1   = new ReadInstructions();
        ri1.fromFile(rsa_private_key);
        String privateExponent = ri1.getAttributes().getPrivateExponent();
        String modulus         = ri1.getAttributes().getModulus();

        String decryptedText = rsa.decrypt(rsa.getEncryptedData(), rsa.getPrivateKey(privateExponent, modulus));
        System.out.println(decryptedText);

    }

    private String updateEncryptAttributes() throws Exception {
        Attributes attributes = new Attributes();
        attributes.setDescription("Crypted file");
        attributes.setMethod("RSA");
        attributes.setFileName(rsa_input);
        attributes.setData(rsa.getEncryptedData());
        new WriteInstructions(attributes).toFile(rsa_output);
        return attributes.getData();

    }



}
