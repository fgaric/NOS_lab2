package Utils;

import AES_3DES_RSA_SHA.Des3x;
import Formated_key_print.Attributes;
import Formated_key_print.ReadInstructions;
import Formated_key_print.WriteInstructions;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

@SuppressWarnings("Duplicates")
public class Des3xUtil {
    private String des3x_input;
    private String des3x_output;
    private String des3x_key;
    private Des3x des3x;


    public Des3xUtil(String des3x_input, String des3x_output, String des3x_key) {
        this.des3x_input = des3x_input;
        this.des3x_output = des3x_output;
        this.des3x_key = des3x_key;
        this.des3x = new Des3x();
    }

    public String encrypt() throws Exception {
        ReadInstructions readInstructions = new ReadInstructions();
        readInstructions.fromFile(des3x_key);
        String keyLength = readInstructions.getAttributes().getKeyLength();
        String secretKey = readInstructions.getAttributes().getSecretKey();
        String fileText = new String(Files.readAllBytes(Paths.get(des3x_input)));

        des3x.encrypt(fileText, secretKey, "CBC");

        return updateEncryptAttributes();

    }

    public void decrypt() throws Exception {
        ReadInstructions ri = new ReadInstructions();
        ri.fromFile(des3x_output);
        String data_in_Base64 = ri.getAttributes().getData();
        byte[] data_in_byte   = Base64.getDecoder().decode(data_in_Base64);

        ReadInstructions ri1 = new ReadInstructions();
        ri1.fromFile(des3x_key);
        String secretKey  = ri1.getAttributes().getSecretKey();

        String decryptedText = des3x.decrypt(data_in_byte, secretKey, "CBC");
        System.out.println(decryptedText);
    }

    private String updateEncryptAttributes() throws Exception{
        Attributes attributes = new Attributes();
        attributes.setDescription("Crypted file");
        attributes.setMethod("Des3x");
        attributes.setFileName(des3x_input);
        attributes.setData(des3x.getEncryptedData());
        new WriteInstructions(attributes).toFile(des3x_output);

        return attributes.getData();

    }

    public Des3x getDes3x() {
        return des3x;
    }
}
