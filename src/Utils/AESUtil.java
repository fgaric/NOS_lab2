package Utils;

import AES_3DES_RSA_SHA.AES;
import Formated_key_print.Attributes;
import Formated_key_print.ReadInstructions;
import Formated_key_print.WriteInstructions;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

@SuppressWarnings("Duplicates")
public class AESUtil {
    private String aes_input;
    private String aes_output;
    private String aes_key;
    private AES aes;

    public AESUtil(String aes_input, String aes_output, String aes_key) {
        this.aes_input       = aes_input;
        this.aes_output      = aes_output;
        this.aes_key         = aes_key;
        this.aes             = new AES();

    }

    public void encrypt() throws Exception {
        ReadInstructions ri = new ReadInstructions();
        ri.fromFile(aes_key);
        String keyLength  = ri.getAttributes().getKeyLength();
        String secretKey  = ri.getAttributes().getSecretKey();
        String initVector = ri.getAttributes().getInitializationVector();
        String fileText   = new String(Files.readAllBytes(Paths.get(aes_input)));

        aes.encrypt(fileText.getBytes(), secretKey, CoolUtil.hexStringToByteArray(initVector), "CBC");
        updateEncryptAttributes();

    }

    public void decrypt() throws Exception {
        ReadInstructions ri   = new ReadInstructions();
        ri.fromFile(aes_output);
        String data_in_Base64 = ri.getAttributes().getData();
        byte[] data_in_byte   = Base64.getDecoder().decode(data_in_Base64);

        ReadInstructions ri1 = new ReadInstructions();
        ri1.fromFile(aes_key);
        String secretKey     = ri1.getAttributes().getSecretKey();
        String initVector    = ri1.getAttributes().getInitializationVector();

        String decryptedText = aes.decrypt(data_in_byte, secretKey, CoolUtil.hexStringToByteArray(initVector), "CBC");
        System.out.println(decryptedText);

    }

    private void updateEncryptAttributes() throws Exception {
        Attributes attributes = new Attributes();
        attributes.setDescription("Crypted file");
        attributes.setMethod("AES CBC");
        attributes.setFileName(aes_input);
        attributes.setData(aes.getEncryptedData());
        new WriteInstructions(attributes).toFile(aes_output);

    }




}
