package Utils;

public class EnvelopeObject {
    private String C1;
    private String C2;

    public EnvelopeObject(String c1, String c2) {
        C1 = c1;
        C2 = c2;
    }

    public String getC1() {
        return C1;
    }

    public void setC1(String c1) {
        C1 = c1;
    }

    public String getC2() {
        return C2;
    }

    public void setC2(String c2) {
        C2 = c2;
    }
}
