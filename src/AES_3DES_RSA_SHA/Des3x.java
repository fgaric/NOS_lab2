package AES_3DES_RSA_SHA;

import java.security.*;
import java.util.Arrays;
import javax.crypto.*;

@SuppressWarnings("Duplicates")
public class Des3x {
    private byte[] encrypted;

    public Des3x() {
    }


    public byte[] encrypt(String toEncrypt, String encryptionKey, String mode) throws Exception {
        SecureRandom secureRandom = new SecureRandom(encryptionKey.getBytes()); // create a binary key from the argument key (seed)
        KeyGenerator keyGenerator = KeyGenerator.getInstance("DESede");
        keyGenerator.init(secureRandom);
        SecretKey secretKey = keyGenerator.generateKey();

        Cipher cipher = Cipher.getInstance("DESede");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        encrypted = cipher.doFinal(toEncrypt.getBytes());

        return encrypted;
    }

    public String decrypt(byte[] toDecrypt, String decryptionKey, String mode) throws Exception {
        SecureRandom secureRandom = new SecureRandom(decryptionKey.getBytes()); // create a binary key from the argument key (seed)
        KeyGenerator keyGenerator = KeyGenerator.getInstance("DESede");
        keyGenerator.init(secureRandom);
        SecretKey secretKey = keyGenerator.generateKey();

        Cipher cipher = Cipher.getInstance("DESede");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] decrypted = cipher.doFinal(toDecrypt);

        return new String(decrypted);
    }

    public byte[] getEncryptedData() {
        return this.encrypted;
    }

}