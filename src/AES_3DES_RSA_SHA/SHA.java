package AES_3DES_RSA_SHA;

import java.math.BigInteger;
import java.security.MessageDigest;

public class SHA {

    public String shaThisString(String input) throws Exception{
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        byte[] messageDigest = md.digest(input.getBytes());
        BigInteger no = new BigInteger(1, messageDigest);

        String hashText = no.toString(16);

        while (hashText.length() < 32) {
            hashText = "0" + hashText;
        }

        return hashText;
    }
}
