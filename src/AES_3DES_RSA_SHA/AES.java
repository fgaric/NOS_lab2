package AES_3DES_RSA_SHA;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AES {

    public AES() {
    }

    private byte[] IV;
    private byte[] cipherText;
    private SecretKey key2;

    public byte[] encrypt (byte[] plaintext, String key2, byte[] IV, String cipherType) throws Exception {
        Cipher cipher          = Cipher.getInstance("AES/" + cipherType + "/PKCS5Padding");
        SecretKeySpec keySpec  = new SecretKeySpec(key2.getBytes(), "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(IV);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);
        cipherText = cipher.doFinal(plaintext);

        return cipherText;
    }

    public String decrypt (byte[] cipherText, String key2, byte[] IV, String cipherType) throws Exception {
        Cipher cipher          = Cipher.getInstance("AES/" + cipherType + "/PKCS5Padding");
        SecretKeySpec keySpec  = new SecretKeySpec(key2.getBytes(), "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(IV);
        cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);

        return new String(cipher.doFinal(cipherText));
    }

    public byte[] getEncryptedData() {
        return this.cipherText;
    }

}