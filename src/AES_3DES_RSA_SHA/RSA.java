package AES_3DES_RSA_SHA;

import javax.crypto.Cipher;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

public class RSA {
    private byte[] encryptedData;

    public byte[] encrypt(String data, PublicKey publicKey) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        encryptedData = cipher.doFinal(data.getBytes());

        return encryptedData;
    }

    public byte[] encrypt(String data, PrivateKey privateKey) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);
        encryptedData = cipher.doFinal(data.getBytes());

        return encryptedData;
    }

    public String decrypt(byte[] data, PrivateKey privateKey) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);

        return new String(cipher.doFinal(data));
    }

    public String decrypt(byte[] data, PublicKey publicKey) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, publicKey);

        return new String(cipher.doFinal(data));
    }

    public PrivateKey getPrivateKey(String privateExponentString, String modulusString) throws Exception {
        BigInteger modulus            = new BigInteger(modulusString, 16);
        BigInteger privateExponent    = new BigInteger(privateExponentString, 16);
        RSAPrivateKeySpec privateSpec = new RSAPrivateKeySpec(modulus, privateExponent);
        KeyFactory factory = KeyFactory.getInstance("RSA");

        return factory.generatePrivate(privateSpec);
    }

    public PublicKey getPublicKey(String publicExponentString, String modulusString) throws Exception {
        BigInteger modulus          = new BigInteger(modulusString, 16);
        BigInteger publicExponent   = new BigInteger(publicExponentString, 16);
        RSAPublicKeySpec publicSpec = new RSAPublicKeySpec(modulus, publicExponent);
        KeyFactory factory = KeyFactory.getInstance("RSA");

        return factory.generatePublic(publicSpec);

    }

    public byte[] getEncryptedData() {
        return this.encryptedData;
    }
}
